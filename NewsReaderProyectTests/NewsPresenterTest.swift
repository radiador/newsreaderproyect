//
//  NewsPresenterTest.swift
//  NewsReaderProyectTests
//
//  Created by formador on 22/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import XCTest

@testable import NewsReaderProyect

class NewsPresenterTest: XCTestCase {
    
    var presenter: NewsPresenter!
    var dummyService: DummyService!
    var dummyView: DummyView!
    
    override func setUp() {
        
        presenter = NewsPresenter()
        
        dummyService = DummyService()
        dummyView = DummyView()
        
        presenter.service = dummyService
        presenter.view = dummyView
    }
    
    func test_givenAny_whenICallViewDidLoad_thenICallServiceLoadNews() {
        
        //Given
        
        //when
        presenter.viewDidLoad()
        
        //Then
        XCTAssert(dummyService.isCalledLoadNews == true)
    }
    
    func test_givenAny_whenICallViewDidLoadAndIsSucess_thenICallViewLoadNews() {
        
        //Given
        
        //when

        presenter.viewDidLoad()
        
        //Then
        waitForExpectations(timeout: 3)
        XCTAssert(dummyView.isCalledShowNews == true)
    }
    
    func test_givenAny_whenICallLoadNew_thenICallServiceLoadNew() {
        
        //Given
        
        //when
        presenter.loadNew(id: 1)
        
        //Then
        XCTAssert(dummyService.isCalledLoadNew == true)
    }
    
    
    class DummyService: NewsServiceProtocol {
        
        var isCalledLoadNews = false
        var isCalledLoadNew = false

        func loadNews(type: NewsType, success: @escaping ([Int]) -> Void, errorHandler: @escaping (String) -> Void) {
            
            isCalledLoadNews = true
            success([1])
        }
        
        func loadNew(id: Int, success: @escaping (Story) -> Void, errorHandler: @escaping (String) -> Void) {
            
            isCalledLoadNew = true
        }
    }
    
    class DummyView: BaseNewsViewProtocol {
        
        var isCalledShowNews = false
        var isCalledLoadNew = false
        
        let showNewsExpectation = XCTestExpectation(description: "showNewsExpectation")
        
        func showNews(_ news: [Int]) {
            
            isCalledShowNews = true
            showNewsExpectation.fulfill()
        }
        
        func newLoaded(new: Story) {
            
            isCalledLoadNew = true
        }
    }
}
