//
//  Story.swift
//  NewsReader
//
//  Created by formador on 16/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

protocol Story {
    
    var id: Int { get }
    var type: String { get }
    var by: String { get }
    var text: String? { get }
    var url: String { get }
    var score: Int { get }
    var title: String { get }

}

struct BasicStory: Story, Codable {
    
    let id: Int
    let type: String
    let by: String
    var text: String?
    let url: String
    let score: Int
    let title: String
}
