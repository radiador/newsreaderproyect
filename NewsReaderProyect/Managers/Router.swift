//
//  Router.swift
//  NewsReaderProyect
//
//  Created by formador on 20/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation
import UIKit

final class Router {
    
    static let router = Router()
    
    private init() {}
    
    func setUpNewsViewController(_ viewController: NewsViewController) {

        let newService = NewsService.newsService

        let presenter = NewsPresenter()
        presenter.service = newService
        presenter.view = viewController
        
        viewController.presenter = presenter
    }
    
    func setUpTopNewsViewController(_ viewController: TopNewsViewController) {
        
        let newService = NewsService.newsService

        let presenter = TopNewsPresenter()
        presenter.service = newService
        presenter.view = viewController

        viewController.presenter = presenter
    }
    
    func setUpBestNewsViewController(_ viewController: BestNewsViewController) {
        
        let newService = NewsService.newsService

        let presenter = BestNewsPresenter()
        presenter.service = newService
        presenter.view = viewController

        viewController.presenter = presenter
    }
    
    func routeToWebView(withNew new: Story, navigationController: UINavigationController) {
        
        if let webViewViewcontroller = UIStoryboard(name: "First", bundle: nil).instantiateViewController(withIdentifier: "webViewIdentifier") as? WebViewViewController {
            
            webViewViewcontroller.new = new
            
            navigationController.pushViewController(webViewViewcontroller, animated: true)
        }
    }
}
