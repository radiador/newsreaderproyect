//
//  PresenterProtocol.swift
//  NewsReaderProyect
//
//  Created by formador on 21/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

protocol NewsPresenterProtocol {
    
    var service: NewsServiceProtocol? { get }
    var view: BaseNewsViewProtocol? { get}
    
    func loadNew(id: Int)
    func viewDidLoad()
}

extension NewsPresenterProtocol {
    
    func loadNew(id: Int) {
        
        service?.loadNew(id: id, success: { story in
            
            DispatchQueue.main.async {

                self.view?.newLoaded(new: story)
            }
            }, errorHandler: { error in
                
                print("Error al cargar la noticia")
                
        })
    }
}
