//
//  TopNewsPresenter.swift
//  NewsReaderProyect
//
//  Created by formador on 16/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

class TopNewsPresenter: NewsPresenterProtocol  {
    
    var view: BaseNewsViewProtocol?
    var service: NewsServiceProtocol?

    func viewDidLoad() {
        
        service?.loadNews(type: .top, success: { [weak self] (news) in
            
            DispatchQueue.main.async {
                
                self?.view?.showNews(news)
            }
            }, errorHandler: { [weak self] error in
                
                DispatchQueue.main.async {
                    
                    self?.view?.wasErrorLoadingNews()
                }
        })
    }
    
}
