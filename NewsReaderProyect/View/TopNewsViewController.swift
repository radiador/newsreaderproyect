//
//  TopNewsViewController.swift
//  NewsReaderProyect
//
//  Created by formador on 16/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class TopNewsViewController: BaseNewsViewController {
    
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var filterSearchBar: UISearchBar!

    var presenter: TopNewsPresenter?

    override func viewDidLoad() {
        
        tableView = newsTableView
        super.viewDidLoad()
        
        presenter?.viewDidLoad()
    }
    
    override func getPresenter() -> NewsPresenterProtocol? {
        
        return presenter
    }
}

