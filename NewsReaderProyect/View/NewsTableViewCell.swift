//
//  NewsTableViewCell.swift
//  NewsReaderProyect
//
//  Created by formador on 16/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    func configureCell(withNew new: Story) {
        
        titleLabel.text = new.title
        detailLabel.text = new.text
        scoreLabel.text = "\(new.score)"
        
        //TODO: Crear vista personalizada para los puntos

    }
    
    func clearCell() {
       
        titleLabel.text = ""
        detailLabel.text = ""
        scoreLabel.text = ""
    }
    
}
