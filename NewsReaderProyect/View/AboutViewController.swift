//
//  AboutViewController.swift
//  NewsReaderProyect
//
//  Created by formador on 22/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func changeImageButtonAction(_ sender: Any) {
        
        let otherImage = UIImage(named: "Background2")
        
        UIView.animate(withDuration: 5, animations: {
            
            self.backgroundImageView.alpha = 0
        }) { completed in

            self.backgroundImageView.image = otherImage
            
            UIView.animate(withDuration: 5) {
                
                self.backgroundImageView.alpha = 1
            }
        }
    }
}
