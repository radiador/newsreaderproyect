//
//  WebViewViewController.swift
//  NewsReaderProyect
//
//  Created by formador on 27/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var new: Story? 
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = new?.title
        if let urlText = new?.url, let url = URL(string: urlText) {
            
            webView.load(URLRequest(url: url))
        }
    }

}
