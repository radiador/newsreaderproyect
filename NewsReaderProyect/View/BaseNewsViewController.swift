//
//  BaseNewsViewController.swift
//  NewsReaderProyect
//
//  Created by formador on 20/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

protocol BaseNewsViewProtocol {
    
    func showNews(_ news: [Int])
    func newLoaded(new: Story)
    func wasErrorLoadingNews()
}

class BaseNewsViewController: UIViewController, BaseNewsViewProtocol {
    
    static let newsCellIdentifier = "newsCellIdentifier"
    
    var tableView: UITableView?
    var idNews: [Int]?
    var news = [Int: Story]()
    
    var originalIdNews: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.separatorStyle = .none
        
        tableView?.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: BaseNewsViewController.newsCellIdentifier)
        
        tableView?.dataSource = self
        tableView?.delegate = self
    }
    
    func showNews(_ news: [Int]) {
        
        if let activityViewIndicator = view.viewWithTag(10) as? UIActivityIndicatorView {
            activityViewIndicator.stopAnimating()
        }
        
        idNews = news
        originalIdNews = news
        tableView?.reloadData()
    }
    
    func newLoaded(new: Story) {
        
        news[new.id] = new
        tableView?.reloadData()
    }
    
    func wasErrorLoadingNews() {
        
        if presentedViewController == nil {
            
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            let alertDialog = UIAlertController(title: "Atención", message: "No se ha podido cargar las noticias", preferredStyle: .alert)
            
            alertDialog.addAction(okAction)
            
            present(alertDialog, animated: true, completion: nil)
            
        }
    }
    
    
    func getPresenter() -> NewsPresenterProtocol? { return nil }
}

extension BaseNewsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return idNews?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BaseNewsViewController.newsCellIdentifier, for: indexPath) as? NewsTableViewCell
        
        if let idNew = idNews?[indexPath.row] {
            
            if let new = news[idNew] {
                
                cell?.configureCell(withNew: new)
            } else if let presenter = getPresenter() {
                
                cell?.clearCell()
                presenter.loadNew(id: idNew)
            }
        }
        
        return cell ?? NewsTableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let idNew = idNews?[indexPath.row] {
            
            if let new = news[idNew], let navigationcontroller = navigationController {
                
                Router.router.routeToWebView(withNew: new, navigationController: navigationcontroller)
            }
        }
    }
}

extension BaseNewsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            
            idNews = originalIdNews
            self.tableView?.reloadData()
            return
        }
        
        let idsFilterd = news.filter{ $0.value.title.lowercased().contains(searchText.lowercased()) }.keys
        
        idNews = originalIdNews?.filter{ idsFilterd.contains($0)}
        
        self.tableView?.reloadData()
    }
}

