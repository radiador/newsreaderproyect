//
//  NewsService.swift
//  NewsReaderProyect
//
//  Created by formador on 20/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

enum NewsType {
    case news, top, best
}

protocol NewsServiceProtocol {
    
    func loadNews(type: NewsType,  success: @escaping ([Int]) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void)
    func loadNew(id: Int, success: @escaping (Story) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void)
}

final class NewsService: NewsServiceProtocol {
    
    static let newsService = NewsService()
    
    private init() {}
    
    func loadNews(type: NewsType, success: @escaping ([Int]) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void){
        
        var typePath: String?
        
        switch type {
        case .news:
            typePath = "newstories"
        case .top:
            typePath = "topstories"
        case .best:
            typePath = "beststories"
        }
        
        guard let typeNewsPath = typePath, let urlServer = URL(string: "https://hacker-news.firebaseio.com/v0/\(typeNewsPath).json") else { return }
        
        //Se puede usar el singleton por defecto de URLSession
        URLSession.shared.dataTask(with: urlServer) { data, response, error in
            
            if let error = error {
                errorHandler(error.localizedDescription)
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                
                if let news = try? JSONDecoder().decode([Int].self, from: data) {
                    
                    success(news)
                }
            } else {
                
                errorHandler("No se han podido recuperar las noticias")
            }
            
        }.resume()
    }
    
    func loadNew(id: Int, success: @escaping (Story) -> Void, errorHandler: @escaping (_ errorDescription: String) -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "hacker-news.firebaseio.com"
        urlComponents.path = "/v0/item/\(id).json"
        
        guard let urlServer = urlComponents.url else { return }
        
        URLSession.shared.dataTask(with: urlServer) { data, response, error in
         
            if let error = error {
                errorHandler(error.localizedDescription)
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                
                if let new = try? JSONDecoder().decode(BasicStory.self, from: data) {
                    
                    success(new)
                }
            }
            
        }.resume()
        
    }
    
}
